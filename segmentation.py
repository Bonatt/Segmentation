import numpy as np
import cv2 as cv
import imutils, sys, os, time
from imutils.video import FPS
from scipy import ndimage
from skimage.feature import peak_local_max
from skimage.morphology import watershed
from scipy import stats


#from doAndShow import DoAndShow, imstack
sys.path.append('/home/joshua/FunCode/MachineLearning/ComputerVision/Helpers')
from helpers import *
from doAndShow import DoAndShow, imstack

#FILEPATH = 'Data/vtest.avi'; FILEPATH_OUT='Results/vtest_out.avi'
#FILEPATH = 'Data/vtest.avi'; FILEPATH_OUT='Results/vtest_out_quad.avi'
#FILEPATH = 'Data/vtest.avi'; FILEPATH_OUT='Results/vtest_foreground.avi' 
#FILEPATH = 'Results/vtest_out.avi';
#FILEPATH = 'Data/TownCentreXVID.avi'; FILEPATH_OUT='Results/TownCentreXVID_out.avi'
FILEPATH = 'Data/TownCentreXVID.avi'; FILEPATH_OUT='Results/TownCentreXVID_out_quad.avi'

LOOP = True
WRITE = False
VIEW_ONLY = False
SHOW_INFO = True



class segmentation():
    def __init__(self, background_subtractor, k=5, **kwargs):
        '''Initialize if necessary, pre frames'''

        self.k = k
        #self.kernel = np.ones((k,k))
        self.kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (k,k))

        if background_subtractor == 'mog':
            background_subtractor = cv.bgsegm.createBackgroundSubtractorMOG(**kwargs)
        elif background_subtractor == 'mog2':
            background_subtractor = cv.createBackgroundSubtractorMOG2(**kwargs)
        elif background_subtractor == 'mgm':
            background_subtractor = cv.bgsegm.createBackgroundSubtractorGMG(**kwargs)
        elif background_subtractor == 'knn':
            background_subtractor = cv.createBackgroundSubtractorKNN(**kwargs)
        else:
            print('background_subtractor = {mog, mog2, mgm, knn}')
            return

        self.background_subtractor = background_subtractor
        self.hog = cv.HOGDescriptor()
        self.orb = cv.ORB_create()

        # Create some random colors
        self.color = np.random.randint(0, 255, (100,3))


    def detect_blobs(self, im, im2show):
        # Setup SimpleBlobDetector parameters.
        params = cv.SimpleBlobDetector_Params()

        # Change thresholds
        #params.minThreshold = 10;
        #params.maxThreshold = 200;

        # Filter by Area.
        params.filterByArea = True
        params.minArea = 0.001*np.prod(im.shape)

        # Filter by Circularity
        params.filterByCircularity = False
        params.minCircularity = 0.1

        # Filter by Convexity
        params.filterByConvexity = False
        params.minConvexity = 0.87

        # Filter by Inertia
        params.filterByInertia = False
        params.minInertiaRatio = 0.01

        detector = cv.SimpleBlobDetector_create(params)
        keypoints = detector.detect(im)
        self.keypoints = keypoints

        im_with_keypoints = cv.drawKeypoints(im2show, self.keypoints, np.array([]), cvRED,
                                             cv.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

        return keypoints, im_with_keypoints


    def do(self, frame, **kwargs):
        '''Do during frames'''

        #frame = cv.GaussianBlur(frame, (self.k, self.k), 0)
        framea = cv.cvtColor(frame, cv.COLOR_BGR2BGRA)
        framea[:,:,3] = 0

        blank = np.zeros(framea.shape, np.uint8)
        self.blank = blank

        # Thresholding removes shadows
        # Detecting shadows and thresholding is better than detecting without shadows
        # Open = remove white dot noise; close = remove black dot noise
        fg = self.background_subtractor.apply(frame)
        _, fg = cv.threshold(fg, 200, 255, cv.THRESH_BINARY)
        fg = cv.morphologyEx(fg, cv.MORPH_OPEN, self.kernel)
        fg = cv.morphologyEx(fg, cv.MORPH_CLOSE, self.kernel)
        self.fg = fg

        contours, hierarchy = cv.findContours(fg, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
        contours = [i for i in contours if cv.contourArea(i) >= np.prod(frame[:2].shape)*0.05]
        Contours = cv.drawContours(blank.copy(), contours, -1, (0,255,0,1), -1)
        Contours2 = cv.drawContours(blank.copy(), contours, -1, (0,255,0,1), 1)
    
        # Color fill, color border
        seg = cv.addWeighted(framea, 1, Contours, 0.5, 0)
        seg = cv.add(seg, Contours2)
        seg = cv.cvtColor(seg, cv.COLOR_BGRA2BGR)
        self.seg = seg
    
        # Color fill, black border (overwrites above color)
        # Not using because it looks worse than I thought.
        '''
        seg = cv.addWeighted(framea, 1, Contours, 0.5, 0)
        Contours3 = cv.drawContours(blank.copy(), contours, -1, (1,1,1,1), 1)
        Contours3 = ~Contours3 - 254
        seg = seg * Contours3
        '''

        ### Good Features To Track
        '''
        self.gray = cv.cvtColor(frame, cv.COLOR_BGRA2GRAY)
        corners = cv.goodFeaturesToTrack(self.gray, 25, 0.01, 10).astype(int)
        for i in corners:
            x, y = i.ravel()
            cv.circle(self.seg, (x,y), 3, 255, -1)
        '''

        ### Detect blobs
        #''' 
        keypoints, seg = self.detect_blobs(~fg, cv.cvtColor(seg, cv.COLOR_BGRA2BGR)) 
        n_keypoints = len(keypoints)
        p = (418, 233)
        cv.putText(self.seg, str(n_keypoints), p, cv.FONT_HERSHEY_SIMPLEX, 1, BLACK)
        #'''

        ### Background image
        self.bg = self.background_subtractor.getBackgroundImage()
   
        ### HOG
        '''
        h = self.hog.compute(fg)
        self.h = h
        '''

        ### Edges
        #edges = Canny(fg)
        #self.edges = edges
        edges = cv.drawContours(blank.copy(), contours, -1, WHITE, 1)
        self.edges = cv.cvtColor(edges, cv.COLOR_BGRA2BGR)


        ### Euclidian Distance Transform
        #D = ndimage.distance_transform_edt(fg.copy())
        #localMax = peak_local_max(D, indices=False, min_distance=0, labels=fg.copy()) 
        #markers = ndimage.label(localMax.copy(), structure=np.ones((3, 3)))[0]
        #labels = watershed(-D, markers.copy(), mask=fg.copy())
        #self.D = D
        #self.localMax = localMax
        #self.markers = markers
        #self.labels = labels
        labels, self.nlabels = ndimage.label(self.fg)
        self.labels = cv.cvtColor(cv.normalize(labels, None, 0, 255, cv.NORM_MINMAX, cv.CV_8U), cv.COLOR_GRAY2BGR)

        ### Black edges over background
        self.ghosts = self.edges*self.bg + self.bg


        ### Sparse Optical flow
        # opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_video/py_lucas_kanade
        try:
            # Previous frame. Check if frame0 exists. If not, skip optical flow and do next (not first) frame.
            self.frame0

            # Parameters for ShiTomasi corner detection
            #feature_params = dict(maxCorners=100, qualityLevel=0.03, minDistance=7, blockSize=7)
            #self.gray0 = cv.cvtColor(self.frame0, cv.COLOR_BGRA2GRAY)
            #p0 = cv.goodFeaturesToTrack(self.gray0, **feature_params)

            # Parameters for Lucas Kanade optical flow
            self.gray = cv.cvtColor(self.frame, cv.COLOR_BGRA2GRAY)
            kwargs = dict(winSize=(15,15), maxLevel=2, 
                             criteria=(cv.TERM_CRITERIA_EPS|cv.TERM_CRITERIA_COUNT, 10, 0.03))
            p, st, err = cv.calcOpticalFlowPyrLK(self.gray0, self.gray, self.p0, None, **kwargs)

            # Select good points
            good_new = p[st==1]
            good_old = self.p0[st==1]

            # draw the tracks
            for i, (new, old) in enumerate(zip(good_new, good_old)):
                a, b = new.ravel()
                c, d = old.ravel()
                self.mask = cv.line(self.mask, (a, b), (c,d), self.color[i].tolist(), 2)
                #frame = cv.circle(self.frame, (a, b), 5, self.color[i].tolist(), -1)
                #print(mask.shape)
            self.flow = cv.add(self.mask, cv.cvtColor(self.gray, cv.COLOR_GRAY2BGR))

            # Now update the previous frame and previous points
            self.gray0 = self.gray
            self.p0 = good_new.reshape(-1, 1, 2)

        except AttributeError as e:
            #print(e)
            self.frame0 = self.frame
            self.gray0 = cv.cvtColor(self.frame0, cv.COLOR_BGRA2GRAY)
            # Shi-Tomasi Corner Detector
            kwargs = dict(maxCorners=100, qualityLevel=0.03, minDistance=7, blockSize=7)
            self.p0 = cv.goodFeaturesToTrack(self.gray0, **kwargs)
            # ORB
            #kp = self.orb.detect(self.frame0, None) 
            #self.p0, _ = self.orb.compute(self.frame0, kp)
            self.flow = self.frame0
            self.mask = np.zeros(frame.shape, np.uint8)


        ### Dense Optical Flow
        try:
            self.pp0
            self.pp1 = cv.cvtColor(self.frame, cv.COLOR_BGR2GRAY)
            flow2 = cv.calcOpticalFlowFarneback(self.pp0, self.pp1, None, 0.5, 3, 15, 3, 5, 1.2, 0)
            mag, ang = cv.cartToPolar(flow2[...,0], flow2[...,1])
            self.hsv[...,0] = ang*180/np.pi/2
            self.hsv[...,2] = cv.normalize(mag, None, 0, 255, cv.NORM_MINMAX)
            self.rgb = cv.cvtColor(self.hsv, cv.COLOR_HSV2BGR)
            self.pp0 = self.pp1
        except AttributeError as e:
            self.pp0 = cv.cvtColor(self.frame, cv.COLOR_BGR2GRAY)
            self.hsv = np.zeros_like(self.frame)
            self.hsv[...,1] = 255
            self.rgb = cv.cvtColor(self.hsv, cv.COLOR_HSV2BGR)
            


        ### im2show
        im2show = imstack([[self.seg, self.labels], [self.ghosts, self.flow]])
        im2show = imstack([[self.seg, self.labels], [self.rgb, self.flow]])
        #im2show = self.seg
        #im2show = self.fg
        #im2show = self.labels

        return im2show



class segmentation_median():
   
    def __init__(self, history=20): 
        self.history = history
        self.bg_frames = []
        #self.bg = self.bg_frames[-history:]

    def do(self, frame):
        # BGR2GRAY
        frame = np.median(frame, axis=2)

        self.bg_frames.append(frame)
        #self.bg_frames = self.bg_frames[-self.history:]
        if len(self.bg_frames) >= self.history:
            self.bg_frames.pop(0)
        bg = np.median(np.array(self.bg_frames), axis=0)
        #bg = stats

        fg1 = frame - bg
        #self.fg2 = self.bg - self.frame

        im2show = np.hstack([frame, bg])#, fg1])
        #im2show = self.fg2


        ### Blurs. Fun.
        '''
        # BGR2GRAY
        self.frame = np.median(frame, axis=2)
        # Frame count starts at 1
        if self.nthframe == 1:
            self.bg = self.frame
        else:
            self.bg = np.median(np.array([self.bg, self.frame]), axis=0)
        im2show = self.bg
        '''

        
        return im2show




   
 
#process = segmentation('mog', history=200, nmixtures=5, backgroundRatio=0.7, noiseSigma=0)
process = segmentation('mog2', history=5000, varThreshold=16., detectShadows=True)
#process = segmentation('mgm', initializationFrames=120, decisionThreshold=0.8)
#process = segmentation('knn', history=500, dist2Threshold=400., detectShadows=True)
#process = None
#process = segmentation_median()



#keypoints, im_with_keypoints = blobs(~fg_mog2)
#print(keypoints)
#cvimshow(im_with_keypoints)



if __name__ == '__main__':
    DoAndShow(process=process, filepath=FILEPATH, filepath_out=FILEPATH_OUT,
              loop=LOOP, write=WRITE, view_only=VIEW_ONLY, show_info=SHOW_INFO, 
              cap_width_max=512, cap_fps_max=100)

if WRITE:
    try:
        cv.imwrite(FILEPATH_OUT.replace('out.avi', 'background.jpg'), process.bg)
    except:
        pass
