import numpy as np
import cv2 as cv
import imutils
import sys
sys.path.append('/home/joshua/FunCode/MachineLearning/ComputerVision/')
from helpers import *

filepath = './vtest.avi'

cap = cv.VideoCapture(filepath)
if not cap.isOpened():
    print('Error loading video stream, file.')



rho = 1
theta = np.pi/180
minLineLength = 0.05 * 960
maxLineGap = 5
threshold = 100#int(minLineLength) + maxLineGap

k = 3
kernelsize = (k,k)
kernel = np.ones(kernelsize)
kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (5,5))



fgbg_mog = cv.bgsegm.createBackgroundSubtractorMOG()
fgbg_mog2 = cv.createBackgroundSubtractorMOG2()
fgbg_gmg = cv.bgsegm.createBackgroundSubtractorGMG()


n_contours = []

while cap.isOpened():
    ret, frame = cap.read()
    if ret:

        frame = imutils.resize(frame, width=800)
        gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

        ### Hough Lines
        #gray = cv.GaussianBlur(gray, kernelsize, 0)
        #edges = Canny(gray)
        #edges = cv.GaussianBlur(edges, kernelsize, 0)
        #lines = cv.HoughLinesP(edges, rho=rho, theta=theta, threshold=threshold,
        #                minLineLength=minLineLength, maxLineGap=maxLineGap)
        #for x1,y1,x2,y2 in lines[:,0]:
        #   cv.line(frame, (x1, y1), (x2, y2), GREEN, 1)
        #cv.imshow(filepath, frame)

        ### Median Segmentation
        #median = cv.medianBlur(gray, 501)
        #ret, thresh = cv.threshold(gray-median, 200, 255, cv.THRESH_BINARY)
        #cv.imshow(filepath, thresh)

        ### MOG
        fg_mog = fgbg_mog.apply(gray)
        fg_mog2 = fgbg_mog2.apply(frame)
        fg_gmg = fgbg_gmg.apply(frame)
        #fg_all = cv.cvtColor(np.hstack([fg_mog, fg_mog2, fg_gmg]), cv.COLOR_GRAY2BGR)

        _, fg_mog2 = cv.threshold(fg_mog2, 200, 255, cv.THRESH_BINARY) 
        fg_mog2 = cv.morphologyEx(fg_mog2, cv.MORPH_OPEN, kernel) 
        #fg_mog2 = cv.morphologyEx(fg_mog2, cv.MORPH_CLOSE, kernel)

        labels, connectivity = cv.connectedComponents(fg_mog)
      
        contours, hierarchy = cv.findContours(fg_mog2, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
        contours = [i for i in contours if cv.contourArea(i) >= np.prod(gray.shape)*0.001]
        n_contours.append(len(contours))       
        recent = 10
        n_contours_recent = max(n_contours[-recent:])
        fg_mog_color = cv.cvtColor(fg_mog, cv.COLOR_GRAY2BGR)
        Contours = cv.drawContours(frame.copy(), contours, -1, GREEN, 1)
        #cv.putText(Contours, str(n_contours_recent), (20,40), cv.FONT_HERSHEY_SIMPLEX, 1, GREEN, 1)
        #cv.imshow(filepath, Contours)
        
        cv.putText(frame, str(labels), (20,40), cv.FONT_HERSHEY_SIMPLEX, 1, GREEN, 1)
        cv.imshow(filepath, fg_mog2)

        ### imshow with pause
        key = cv.waitKey(1) & 0xFF
        key_pause = ord('p')
        if key == key_pause:
            while True:
                cv.imshow(filepath, Contours)
                key2 = cv.waitKey(1) & 0xFF
                if (key2 < 128): break
            if (key2 < 128) and (key2 != key_pause): break
        if (key < 128) and (key != key_pause): break

        
        
    else: break

cap.release()
cv.destroyAllWindows()
