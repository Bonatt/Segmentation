# Video Segmentation via Mixture of Gaussians, and Other Things

At least two things happened here: 
1. Played around with various video segmentation techniques; just kind of having fun.
2. Created a video player of sorts to better view my video handywork.

Here's a spolier:

![](Results/vtest_out.gif)


### What is Video Segmentation?

Segmentation is a class of techniques employed to discern objects within an image or video.
Often this means a creating semantic distinction between dynamic foreground objects and a static background. 
Here I investigate foreground/background segmentation techniques using a static camera, i.e., 
the camera position does not change (otherwise this is a much more difficult problem, perhaps to be discussed later).

The simplest case involves saving an initial background image without foreground object(s), 
and upon subtraction from a new image with foreground object(s), would yield all foreground object(s). 
This is literally background subtraction. However, if a background image is not available or not practical (dynamic lighting conditions, etc.), 
then other methods must be used. This really just means background subtraction as a function of time.

To first order, objects of interest (foreground) interact within some scene (background). 
If one can identify these objects, one can begin to understand them. 
At the very least this means idenfitication in the context of security, e.g., or labelled data creation.
I dug into this after thinking about how to auto-label a dataset for tasks better-suited for deep learning.
These labels are usually hand-labelled to pixel precision, 
and perhaps later further labelled by semi-supervised models.
Here I figured I could learn a bit of traditional computer vision segmentation instead 
(because one could readily segment the Town Centre dataset, e.g., via modern CNN semantic segmentation
methods, but that's not the goal here).


### Techniques

To be continued.


### Results

A background image may be constructed after sucessful video segmentation.
Now this image may be employed as if we had this initial frame all along 
(again, assuming static lighting, etc.):

![](Results/vtest_background.jpg)

The real prize is a labelled foreground video, below. You can see it doesn't do too well -- 
the individual blobs are not tracked, and thus blob labels flicker/swap throughout the video.
Counting and tracking are the intuitive next steps here.

![](Results/vtest_foreground.gif)

To be continued.
